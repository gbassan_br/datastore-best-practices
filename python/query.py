import uuid
import argparse
import datetime
import time
import random
import json
from google.cloud import datastore
#creating the datastore client
client = datastore.Client()
#initializing cursors variables
cursor = ""
lastcursor = ""
recordnum = 0
#setting the limit for cursor pagination
limit = 100
#query full entities of the 30% of keys returned on the cursor page
percent = int(limit * 0.30)
while cursor is not None:
    #setting kind
    query = client.query(kind='Person')
    #setting projection like SELECT COLUMN
    query.projection = ["name"]
    #setting where clause
    query.add_filter("sex", "=", "M")
    #if order is important uncomment this line
    #query.order = ["name"]
    #enable return of keys_only to pay for 1 Read op and small operation for the number of results
    query.keys_only()
    #create the query with the cursor, which will updated later with next page. Also the query limit is set
    query_iter = query.fetch(start_cursor=cursor, limit=limit)
    #getting the cursor page
    page = next(query_iter.pages)
    #converting the page to a list to iterate over
    tasks = list(page)
    lastcursor = cursor
    #update the cursor to get the next page in the next iteration
    cursor = query_iter.next_page_token
    print("Cursor: {}".format(cursor))
    print("Records in this batch: {}".format(len(tasks)))
    #iterate over the pages from the cursor
    for entity in tasks:
        #print only keys
         print(entity)
    #just recording the number of entities returned on the cursor page
    recordnum += len(tasks)
    print("Processed Total of {} entities so far".format(recordnum))
    print("Random query some entities")
    #just random query 30% of the returned keys over the first query page
    for index in range(percent):
        #setting the kind
        queryent = client.query(kind=tasks[random.randrange(0,limit)].key.kind)
        #filter the key with the chosen random key
        queryent.add_filter('__key__', "=", tasks[random.randrange(0,limit)].key)
        #execute the query
        result = queryent.fetch()
        #iterate the results
        item = next(result.pages)
        #convert the entity to list
        entity = list(item)
        #print the whole entity that is charged as a 1 Read Op
        print(json.dumps(entity, indent=4, sort_keys=True))

    time.sleep(0.3)