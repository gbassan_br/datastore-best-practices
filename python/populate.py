##################################################################################
## Example to populate an Datastore Kind with fake person data
## This example was created to demonstrate the audit logs for Datastore/Firestore
## usage: 
# install python libraries: pip install faker google.cloud.datastore
# authorize the SDK: gcloud auth application-default login
# 


import uuid
import argparse
import datetime
from faker import Faker
from google.cloud import datastore
from faker.factory import Factory

client = datastore.Client()
parser = argparse.ArgumentParser(description='Insert Fake data on Datastore Entity.')
group = parser.add_mutually_exclusive_group()
group.add_argument('-q', '--query', action="store_true",
                    help='Create random queries to populate key visualizer and/or BQ with audit logs.')
group.add_argument('-i', '--insert', action="store_true",
                    help='Create random inserts to populate Datastore. -n --number is required')                    
parser.add_argument('-n','--number', dest='number', metavar='N', type=int,
                    help='The number of entities to be inserted.')
parser.add_argument('-k','--kind', dest='kind', metavar='K', type=str,
                    help='The kind where entities will be inserted/queried.')
args = parser.parse_args()

Faker = Factory.create
fake = Faker()
fake.seed(0)

parser.add_argument_group
print(args.number)
if(args.query and args.kind is not None):
    cursor = ""
    recordnum = 0
    while cursor is not None:
        query = client.query(kind=args.kind)
        query_iter = query.fetch(start_cursor=cursor, limit=100)
        page = next(query_iter.pages)
        tasks = list(page)
        cursor = query_iter.next_page_token
        print(cursor)
        for entity in tasks:
            print("username in this batch: {}".format(entity["username"]))
        recordnum += len(tasks)
        print("Processed batch of {} entities".format(recordnum))

        

elif(args.insert and args.number is not None and args.kind is not None):
    for index  in range(args.number):
        print("Generating {} records...".format(args.number))
        #generating fake uuid to be used as a entity key
        uuid = fake.uuid4()
        #generating fake person profile
        record = fake.simple_profile()
        #converting datatime to RFC 3339 
        record['birthdate'] = record['birthdate'].isoformat()
        
        key = client.key(args.kind, str(uuid))
        entity = datastore.Entity(key=key)
        entity.update(record)
        client.put(entity)
        print("Inserted record {}, with uuid {}, and  with username {}.".format(index, uuid, record['username']))
        # print(record)
elif(args.kind is None):
        parser.error("--kind is always required")
else:
    parser.error("--insert -i requires --number -n N")
