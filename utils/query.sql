SELECT  
resource.labels.method,
protopayload_auditlog.serviceName,
protopayload_auditlog.resourceName,
protopayload_auditlog.authenticationInfo.principalEmail,
protopayload_auditlog.authenticationInfo.serviceAccountKeyName,
protopayload_auditlog.requestMetadata.callerIp,
case when JSON_EXTRACT(protopayload_auditlog.requestJson,'$.gqlQuery.queryString')  is not null 
      then JSON_EXTRACT(protopayload_auditlog.requestJson,'$.gqlQuery.queryString')
      else JSON_EXTRACT(protopayload_auditlog.requestJson,'$.query')
  end as query,
protopayload_auditlog.numResponseItems,
timestamp as eventime,
receiveTimestamp,
insertId
FROM `<project-id>.<dataset-name>.cloudaudit_googleapis_com_data_access` as t
WHERE
   protopayload_auditlog.methodName in ('google.datastore.v1.Datastore.RunQuery','google.datastore.v1.Datastore.Commit','google.datastore.v1.Datastore.Lookup')
AND
    -- if you would like to filter recent logs, uncomment this next line and set the INERVAL X MINUTES/SECONDS
   receiveTimestamp > timestamp_sub(current_timestamp(), INTERVAL 60 MINUTE)
order by numResponseItems desc