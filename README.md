# datastore-best-practices

## Introduction

Some Google Cloud Firestore/Datastore customers notice an increase of the read operation charges in their Firestore Tables, specially when the application increase its load/users. Usually the read charges are higher than the other charges in Firestore/Datastore. To help optimize costs and provide best practices to query Firestore/Datastore, this example was created to provide a functional application for demonstration purposes.

## Objective

The objective of this repo and README is help Firestore/Datastore administrators to enable Audit Logs in order to help address the most expensive read operations queries and help the development team to change it into the application.
In addition, this repo includes some best practices approaches around query data in Firestore/Datastore. It leverages some techniques such as CURSORS, Keys-Only and Projection queries to reduce the read operations charge and leverage small operations. This can also be used in paralel with Lazy loading data from Firestore while the application is loading or the use case permits.

## Step-by-step Guide

### Pre-reqs

1. Install gcloud

1. Install python

1. Install pip

1. Install pyenv

### Enable Firestore/Datastore Audit Logs
1. In the Google Cloud console, navigate to the **IAM & Admin** page and into **Audit Logs**

1. Entry `Firestore` in the fiter field

1. Select the `Firestore/Datastore API`

1. On the right side select `Data Read` and `Data Write`

    ![Audit Logs Enable](images/screen1.png)

1. Click Save.

### Route Firestore audit logs to BigQuery

1. Navigate to the **Logging** page in the Google Cloud console, into the **Log Router**

1. Click `Create Sink` in the upper portion of the console.

1. In the `Sink details`, type `AuditLogs_Sink` in the **Sink Name**. Click `Next`

1. In the `Sink Destination`, choose `BigQuery dataset` in the **Select sink service**

1. Choose `Create new BigQuery dataset` 

1. In the opened form, type `firestore_audit_dataset` in the **Dataset ID**

1. Select `Region` in the **Location type**

1. Selection `us-central1` as the  **Region**

1. Leave the default in the other options. Click `Create Dataset`

![BigQuery dataset creation](images/screen2.png)

1. Make sure you check the `Use partitioned tables` checkbox

1. Click Next

1. Paste the following Log Query to be included in the sink. *Remember to include your <gcp-project> into the query*. Click `Next`.

```
resource.type="audited_resource"
protoPayload.serviceName="datastore.googleapis.com"
log_name="projects/<gcp=project>/logs/cloudaudit.googleapis.com%2Fdata_access"
```
1. Click `Create Sink`

![Create Log Sink](/images/screen3.png)

### Populate and Run queries into Firestore/Datastore

1. in a terminal type the following command to set your project. 

```
gcloud config set project <PROJECT_ID>
```
> Remember to change the <PROJECT-ID> to your project.

1. Authorize your gcloud SDK

```
gcloud auth application-default login
```

1. Enable the python virtual environment

```
pyenv shell venv
```
1. Install the required python libraries. (this can take a while)

```
pip install -r python/requirements.txt
```
1. Populate the `Person` kind with `1000` people. (This can take a while as well)

```
python python/populate.py --insert --kind Person --number 1000
```
1. Make a query using the following command. Tis query will scan the entire table, paginating the result in batches of 100 entitites.

```
python python/populate.py --query --kind Person
```
 
### Querying the audit logs in BigQuery

1. Navigate to the `BigQuery` page in the Google Cloud console.

1. Click on the `+` in the upleft corner to open the Query Editor, or by using the `Compose a new Query` button in the welcome page.

1. Copy and paste the query below into the editor. (this query is also available into the `utils` folder in the `query.sql`)

> Make sure you change the `<project-id>` and the `<dataset-name>` to your actual values.

```sql
SELECT  
resource.labels.method,
protopayload_auditlog.serviceName,
protopayload_auditlog.resourceName,
protopayload_auditlog.authenticationInfo.principalEmail,
protopayload_auditlog.authenticationInfo.serviceAccountKeyName,
protopayload_auditlog.requestMetadata.callerIp,
case when JSON_EXTRACT(protopayload_auditlog.requestJson,'$.gqlQuery.queryString')  is not null 
      then JSON_EXTRACT(protopayload_auditlog.requestJson,'$.gqlQuery.queryString')
      else JSON_EXTRACT(protopayload_auditlog.requestJson,'$.query')
  end as query,
protopayload_auditlog.numResponseItems,
timestamp as eventime,
receiveTimestamp,
insertId
FROM `<project-id>.<dataset-name>.cloudaudit_googleapis_com_data_access` as t
WHERE
   protopayload_auditlog.methodName in ('google.datastore.v1.Datastore.RunQuery','google.datastore.v1.Datastore.Commit','google.datastore.v1.Datastore.Lookup')
AND
    -- if you would like to filter recent logs, uncomment this next line and set the INERVAL X MINUTES/SECONDS
   receiveTimestamp > timestamp_sub(current_timestamp(), INTERVAL 60 MINUTE)
order by numResponseItems desc
```
1. Click `Run` to execute the query.

![BigQuery Query](/images/screen4.png)

1. Notice the query is getting all read data access operations logs (Firestore RunQuery, Lookup, Commit) and order it ASC by the number of entities returned. This could allow you to identify the most expensive read operations for example, in order to help optimize the scenario costs.

1. In addition, the query is returning the last 60 minutes logs. Generally the audit logs are quickly shown into BigQuery

1. Remember to correct estimate  the costs to enable audit logs in a high volume Firestore/Datastore environment. Some best practices could be used to reduce the impact, while enabling it to investigate query behavior on a cost optimization effrort:
    1. Enable log exclusion in the Default Bucket to the logs being sent to BigQuery. This will allow you to not duplicate the ingestion and storage costs
    1. Enable the audit Logs **only** for a period to have enough data to analize
    1. Enable the audit logs **only** in a low volume hours to reduce the logs being created


### Run the query best practice python file

1. In the `python` folder, there is an python example called `query.py`. This python app uses some approaches to optimize the costs of read data from Firestore/Datastore. Bellow are some examples and explanation of these approaches:
    1.  Query projection: By defining the *columns* to be returned, the query will be charged as a just **one entity** read and the entities returned will be considered as *small operations*

        ```python
        query.projection = ["name"]
        ```
    1. Keys only: By define the query to only return the keys, rather than all the *columns*, the query will be charged as a just **one entity** read and the amount of the keys returned will be considered as *small operations*
        ```python
        query.keys_only()
        ```
    1. Using filters: By defining a *WHERE Clause*, you contribute to reduce the number of entities being returned and therefore charged. This allow yor application to paginate less the resultset and also reduce latency in the query itself.
        ```python
        query.add_filter("sex", "=", "M")
        ```
    1. Using CURSORS. Cursos allow you to paginate the result and enable iteration over it to reduce the returned entity and its costs. Its is a better approach than OFFSET because CURSORS only charge the read entities when a page is read. OFFSET on the other hand only filters out the entities to help the application compute less data, but charge them anyway while read or not.
        ```python
            query_iter = query.fetch(start_cursor=cursor, limit=limit)
            page = next(query_iter.pages)
            cursor = query_iter.next_page_token
        ````
1. Run the example. Open a terminal, if not already opened, and run the follwing command:

    ```
    python python/query.py
    ```
1. Notice that the python app does the following steps
    1. Query the kind `Person``
    1. Filter the `sex` property to return only the entities of the `M` value
    1. Return first only the `KEYS`
    1. On each CURSOR page, full query the details from 30% of the returned keys
    1. Display the JSON accordingly 

1. Query the BigQuery again to see the audit logs

1. Crtl+C to stop the python example app

    

